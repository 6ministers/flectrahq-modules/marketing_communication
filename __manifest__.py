# -*- coding: utf-8 -*-
# Part of Flectra. See LICENSE file for full copyright and licensing details.
{
    'name': "Marketing Communication",
    'version': "1.0",
    'summary': "Build communication mailing campaigns",
    'website': 'https://flectrahq.com',
    'category': "Marketing/Marketing Communication",
    'sequence': 500,
    'depends': ['mass_mailing'],
    'data': [ ],
    'application': True,
    'license': 'LGPL-3',
    'assets': { }
}
